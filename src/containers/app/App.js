import React from 'react';
import './App.css'
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import MainPage from "../mainPage/mainPage";
import Home from "../home/home";
import AboutUs from "../aboutUs/aboutUs";
import Contacts from "../contacts/contacts";

const App = () => {
    return (
            <BrowserRouter>
                <Switch>
                    <Route path="/" exact component={MainPage}/>
                    <Route path="/home" exact component={Home}/>
                    <Route path="/aboutUs" exact component={AboutUs}/>
                    <Route path="/contacts" exact component={Contacts}/>
                </Switch>
            </BrowserRouter>
    );
};

export default App;