import React from 'react';
import './mainPage.css'

const MainPage = props => {
    const onHomeBtnClick = () => {
        props.history.push({
            pathname: '/home'
        });
    };

    const onAboutUsBtnClick = () => {
        props.history.push({
            pathname: '/aboutUs'
        });
    };

    const onContactsBtnClick = () => {
        props.history.push({
            pathname: '/contacts'
        });
    };

    return (
        <div className="container">
            <div className="menu">
                <h1 className="heading">Choose something:</h1>
                <button type="button" className="homeBtn" onClick={onHomeBtnClick}>Home</button>
                <button type="button" className="aboutUsBtn" onClick={onAboutUsBtnClick}>About us</button>
                <button type="button" className="contactsBtn" onClick={onContactsBtnClick}>Contacts</button>
            </div>
        </div>
    );
};

export default MainPage;