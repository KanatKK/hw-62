import React from 'react';
import './aboutUs.css'

const AboutUs = () => {
    return (
        <div className="aboutUs">
            <h1 className="aboutUsHeading">About us</h1>
            <p className="aboutUsTxt">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Adipisci aliquid aperiam asperiores atque beatae culpa cupiditate, dolorum enim
                excepturi illo itaque laboriosam laborum magnam magni modi officia officiis possimus
                praesentium quisquam recusandae repellat repellendus reprehenderit repudiandae rerum sed
                voluptas voluptatem? Adipisci, asperiores commodi cum, doloribus error illum impedit minus
                necessitatibus nesciunt odit quaerat qui quidem rerum sequi vitae! Ad aliquid atque commodi,
                culpa labore repellat repellendus sunt. Accusamus amet, asperiores cumque eaque est et hic
                magni maiores minima molestiae quaerat quam ratione repudiandae soluta ullam voluptas voluptate.
                Accusantium consectetur dolore dolorem dolores eaque earum est eveniet, expedita fugiat, impedit
                incidunt inventore iste iusto magnam molestiae natus nemo qui quos sit sunt. Ab esse omnis qui
                sit sunt veniam veritatis voluptatibus. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                A autem culpa cumque dolor ducimus eius est explicabo facere facilis fugiat ipsam ipsum magni
                modi molestiae molestias, nam neque possimus quam quidem similique sit vitae voluptates!
                Aut cum deserunt ducimus ea est et eveniet harum in ipsa labore nam odit placeat possimus
                rem repudiandae, saepe tenetur! Ad, amet debitis earum eius eos et fugiat, ipsam iste laudantium
                magni molestias non obcaecati quas, quisquam ratione sint tenetur velit? Animi autem,
                deleniti deserunt, dolorum eaque eveniet exercitationem facere id incidunt iste molestias
                necessitatibus neque nobis quaerat quam quo soluta tempora vel. Deleniti mollitia non odio
                possimus quam sapiente ut. Ab, accusantium, blanditiis consequatur deserunt esse eveniet exercitationem
                explicabo fugiat itaque maxime molestiae molestias necessitatibus neque nihil nisi optio porro provident quo,
                repellat rerum sequi sunt vel voluptas. Aliquam cumque distinctio expedita hic illo maxime mollitia omnis
                placeat, quia reiciendis repudiandae saepe suscipit totam.</p>
        </div>
    );
};

export default AboutUs;