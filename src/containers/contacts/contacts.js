import React from 'react';
import './contacts.css';

const Contacts = () => {
    return (
        <div className="contacts">
            <h4>Contacts</h4>
            <p>Numbers: <b>888888, 777777, 666666,</b></p>
            <p>Facebook: <b>SomeFacebook,</b></p>
            <p>Instagram: <b>SomeInstagram,</b></p>
            <p>Mail: <b>Some@mail.com,</b></p>
            <p>WhatsUpp: <b>111111,</b></p>
            <p>Telegram: <b>222222,</b></p>
        </div>
    );
};

export default Contacts;