import React from 'react';
import './home.css'

const Home = () => {
    return (
        <>
            <header className="header"><div className="img"/><p className="headerTxt">Home</p></header>
            <div className="content">
                <h1>Lorem ipsum dolor sit amet!</h1>
                <p className="someTxt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, blanditiis cum dicta dolor doloribus ducimus eveniet facere fugit in incidunt laudantium modi neque officiis provident quae quaerat soluta ullam veniam.</p>
                <div className="cards">
                    <div className="card"><p className="cardTxt">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Accusamus accusantium aperiam, architecto assumenda delectus eveniet exercitationem
                        harum itaque laborum molestiae, nisi odit pariatur tempora temporibus? Lorem ipsum dolor sit amet,
                        consectetur adipisicing elit. Aliquam iste natus nihil, quisquam sed ullam.</p></div>
                    <div className="card"><p className="cardTxt">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        A accusamus assumenda autem consequatur debitis dicta distinctio dolorem, eius est
                        fuga illo ipsa ipsam laboriosam maxime placeat possimus quo saepe totam. Lorem ipsum dolor sit amet,
                        consectetur adipisicing elit. Amet molestiae quibusdam vel vitae!</p></div>
                </div>
                <div className="blocks">
                    <div className="block">
                        <h4>Lorem ipsum dolor</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquid ea, ipsa magni possimus veniam.</p>
                    </div>
                    <div className="block">
                        <h4>Lorem ipsum dolor</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquid ea, ipsa magni possimus veniam.</p>
                    </div>
                    <div className="block">
                        <h4>Lorem ipsum dolor</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquid ea, ipsa magni possimus veniam.</p>
                    </div>
                    <div className="block">
                        <h4>Lorem ipsum dolor</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquid ea, ipsa magni possimus veniam.</p>
                    </div>
                    <div className="block">
                        <h4>Lorem ipsum dolor</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquid ea, ipsa magni possimus veniam.</p>
                    </div>
                </div>
                <h3>Lorem ipsum dolor sit amet, consectetur.</h3>
                <p className="txt">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Adipisci atque debitis id quaerat quia sint sunt unde? Dicta ducimus, eligendi eos
                    esse eveniet in ipsa, itaque maxime molestiae mollitia quaerat quia soluta temporibus
                    ullam ut vitae voluptatum? Accusamus autem blanditiis commodi consectetur corporis cupiditate
                    dignissimos dolore ea eveniet ex fugit laboriosam neque nisi nostrum optio pariatur,
                    reprehenderit unde vel velit veritatis! A dicta dignissimos earum excepturi exercitationem
                    fugiat porro quas.</p>
            </div>
            <footer>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, quod!
            </footer>
        </>
    );
};

export default Home;